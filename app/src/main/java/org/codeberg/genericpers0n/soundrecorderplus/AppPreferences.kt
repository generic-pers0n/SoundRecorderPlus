package org.codeberg.genericpers0n.soundrecorderplus

import android.content.Context
import androidx.preference.PreferenceManager

private const val PREF_BITRATE = "pref_bitrate"
private const val PREF_SAMPLE_RATE = "pref_sample_rate"

fun setPrefBitrate(context: Context, bitrate: Int)  {
    val preferences = PreferenceManager.getDefaultSharedPreferences(context)
    val editor = preferences.edit()
    editor.putString(PREF_BITRATE, "$bitrate")
    editor.apply()
}

fun getPrefBitrate(context: Context): Int {
    val preferences = PreferenceManager.getDefaultSharedPreferences(context)
    val prefValueString = preferences.getString(PREF_BITRATE, "192000")
        ?: "192000"
    return Integer.parseInt(prefValueString)
}

fun setPrefSampleRate(context: Context, sampleRate: Int) {
    val preferences = PreferenceManager.getDefaultSharedPreferences(context)
    val editor = preferences.edit()
    editor.putString(PREF_SAMPLE_RATE, "$sampleRate")
    editor.apply()
}

fun getPrefSampleRate(context: Context): Int {
    val preferences = PreferenceManager.getDefaultSharedPreferences(context)
    val prefValueString = preferences.getString(PREF_SAMPLE_RATE, "44100")
        ?: "44100"
    return Integer.parseInt(prefValueString)
}
