# Sound Recorder Plus

A simple sound recording app implementing Material Design. This is forked from
Easy Sound Recorder, originally available at
https://github.com/dkim0419/SoundRecorder.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/org.codeberg.genericpers0n.soundrecorderplus/)

Or download the latest APK from the [Releases Section](https://codeberg.org/generic-pers0n/SoundRecorderPlus/releases/latest).

## Screenshots

<!-- The screenshots are a little big, so use <img> instead -->
<img src="https://codeberg.org/generic-pers0n/SoundRecorderPlus/raw/branch/main/metadata/en-US/phoneScreenshots/screenshot-record.png" alt="Main recording screen" width="360"/>
<img src="https://codeberg.org/generic-pers0n/SoundRecorderPlus/raw/branch/main/metadata/en-US/phoneScreenshots/screenshot-record-active.png" alt="Active recording on the main screen" width="360"/>
<img src="https://codeberg.org/generic-pers0n/SoundRecorderPlus/raw/branch/main/metadata/en-US/phoneScreenshots/screenshot-saved-recordings.png" alt="Saved recordings screen" width="360"/>
<img src="https://codeberg.org/generic-pers0n/SoundRecorderPlus/raw/branch/main/metadata/en-US/phoneScreenshots/screenshot-recording-player.png" alt="Recording media player" width="360"/>

Credits / Libraries used:

https://github.com/MohammadAG/Android-SoundRecorder