package org.codeberg.genericpers0n.soundrecorderplus.listeners

/** Listen for add/rename items in database
 */
interface OnDatabaseChangedListener {
    fun onNewDatabaseEntryAdded()
}
