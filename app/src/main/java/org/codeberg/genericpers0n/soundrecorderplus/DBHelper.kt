package org.codeberg.genericpers0n.soundrecorderplus

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import org.codeberg.genericpers0n.soundrecorderplus.listeners.OnDatabaseChangedListener

// Public declarations formerly in DBHelper
const val DATABASE_NAME = "saved_recordings.db"
const val DATABASE_VERSION = 1
const val TABLE_NAME = "saved_recordings"

// Public declarations formerly in DBHelperItem
const val COLUMN_NAME_RECORDING_NAME: String = "recording_name"
const val COLUMN_NAME_RECORDING_FILE_PATH: String = "file_path"
const val COLUMN_NAME_RECORDING_LENGTH: String = "length"
const val COLUMN_NAME_TIME_ADDED: String = "time_added"

// Private declarations (formerly in DBHelper)
private const val TEXT_TYPE: String = " TEXT"
private const val COMMA_SEP: String = ","
private const val SQL_CREATE_ENTRIES: String = "CREATE TABLE " + TABLE_NAME + " (" +
    BaseColumns._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
    COLUMN_NAME_RECORDING_NAME + TEXT_TYPE + COMMA_SEP +
    COLUMN_NAME_RECORDING_FILE_PATH + TEXT_TYPE + COMMA_SEP +
    COLUMN_NAME_RECORDING_LENGTH + " INTEGER " + COMMA_SEP +
    COLUMN_NAME_TIME_ADDED + " INTEGER " + ")"

// Unused
// private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TABLE_NAME

class DBHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    companion object {
        // TODO: This annotation is temporary until the rest of the backend code is rewritten in
        // Kotlin. Once that's the case, this annotation should be removed.
        @JvmStatic
        var onDatabaseChangedListener: OnDatabaseChangedListener? = null
    }

    fun getItemAt(position: Int) : RecordingItem {
        val projection = arrayOf(
            BaseColumns._ID,
            COLUMN_NAME_RECORDING_NAME,
            COLUMN_NAME_RECORDING_FILE_PATH,
            COLUMN_NAME_RECORDING_LENGTH,
            COLUMN_NAME_TIME_ADDED
        )

        val cursor = readableDatabase.query(TABLE_NAME, projection, null, null, null, null, null)
        if (cursor.moveToPosition(position)) {
            val item = RecordingItem(
                cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_RECORDING_NAME)),
                cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME_RECORDING_FILE_PATH)),
                cursor.getInt(cursor.getColumnIndexOrThrow(BaseColumns._ID)),
                cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_NAME_RECORDING_LENGTH)),
                cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_NAME_TIME_ADDED))
            )
            cursor.close()
            return item
        } else {
            throw IndexOutOfBoundsException("Index $position doesn't exist")
        }
    }

    fun removeItemWithId(id: Int) {
        val whereArgs = arrayOf("$id")
        writableDatabase.delete(TABLE_NAME, "_ID=?", whereArgs)
    }

    fun getCount(): Int {
        val projection = arrayOf(BaseColumns._ID)
        val cursor = readableDatabase.query(TABLE_NAME, projection, null, null, null, null, null)
        val count = cursor.count
        cursor.close()
        return count
    }

    fun addRecording(recordingName: String, filePath: String, length: Long): Long {
        val contentValues = ContentValues()
        contentValues.put(COLUMN_NAME_RECORDING_NAME, recordingName)
        contentValues.put(COLUMN_NAME_RECORDING_FILE_PATH, filePath)
        contentValues.put(COLUMN_NAME_RECORDING_LENGTH, length)
        contentValues.put(COLUMN_NAME_TIME_ADDED, System.currentTimeMillis())
        val rowId = writableDatabase.insert(TABLE_NAME, null, contentValues)

        onDatabaseChangedListener?.onNewDatabaseEntryAdded()
        return rowId
    }

    fun renameItem(item: RecordingItem, recordingName: String, filePath: String) {
        val contentValues = ContentValues()
        contentValues.put(COLUMN_NAME_RECORDING_NAME, recordingName)
        contentValues.put(COLUMN_NAME_RECORDING_FILE_PATH, filePath)
        writableDatabase.update(TABLE_NAME, contentValues, BaseColumns._ID + "=" + item.id, null)
    }

    // Overrides ///////////////////////////////////////////////////////////////////////////////////
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES)
    }

    // Unused
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) = Unit
    ////////////////////////////////////////////////////////////////////////////////////////////////
}
