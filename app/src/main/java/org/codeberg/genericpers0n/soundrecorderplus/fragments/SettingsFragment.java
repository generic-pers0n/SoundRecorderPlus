package org.codeberg.genericpers0n.soundrecorderplus.fragments;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import org.codeberg.genericpers0n.soundrecorderplus.BuildConfig;
import org.codeberg.genericpers0n.soundrecorderplus.AppPreferencesKt;
import org.codeberg.genericpers0n.soundrecorderplus.R;

/**
 * Created by Daniel on 5/22/2017.
 */

public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, @Nullable String rootKey) {
        // TODO
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        // Bitrate
        EditTextPreference bitratePref = findPreference(getResources().getString(R.string.pref_bitrate_key));
        bitratePref.setOnPreferenceChangeListener((preference, newValue) -> {
            try
            {
                int convertedValue = Integer.parseInt((String) newValue);
                AppPreferencesKt.setPrefBitrate(getActivity(), convertedValue);
                return true;
            } catch(NumberFormatException e)
            {
                return false;
            }
        });

        // Sample rate
        EditTextPreference sampleRatePref = findPreference(getResources().getString(R.string.pref_sample_rate_key));
        sampleRatePref.setOnPreferenceChangeListener((preference, newValue) -> {
            try
            {
                int convertedValue = Integer.parseInt((String) newValue);
                AppPreferencesKt.setPrefSampleRate(getActivity(), convertedValue);
                return true;
            } catch(NumberFormatException e)
            {
                return false;
            }
        });

        // Version
        Preference aboutPref = findPreference(getString(R.string.pref_about_key));
        aboutPref.setSummary(getString(R.string.pref_about_desc, BuildConfig.VERSION_NAME));
        aboutPref.setOnPreferenceClickListener(preference -> {
            LicensesFragment licensesFragment = new LicensesFragment();
            licensesFragment.show(getActivity().getSupportFragmentManager().beginTransaction(), "dialog_licenses");
            return true;
        });
    }
}
