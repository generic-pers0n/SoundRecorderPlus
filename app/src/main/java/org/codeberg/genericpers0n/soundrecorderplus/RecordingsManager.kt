package org.codeberg.genericpers0n.soundrecorderplus

import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.ParcelFileDescriptor
import android.provider.MediaStore
import android.util.Log
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.concurrent.TimeUnit

interface RecordingsListener {
    fun onRecordingsChanged()
}

private object GlobalRecordingsData {
    val recordingUris = ArrayList<Uri>()
    val recordingValues = ArrayList<ContentValues>()
    var initialized = false

    fun init(context: Context) {
        // Guard against reinitialization
        if (initialized) return

        val contentResolver = context.contentResolver

        // Query all files from MediaStore
        val collectionUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            MediaStore.Audio.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
        } else {
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        }

        val projections = mutableListOf(
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.DISPLAY_NAME,
            MediaStore.Audio.Media.DATE_ADDED,
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            projections += arrayOf(
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.DATE_TAKEN,
                MediaStore.Audio.Media.RELATIVE_PATH
            )
        }

        val cursor = contentResolver.query(
            collectionUri,
            projections.toTypedArray(),
            null,
            null,
            MediaStore.Audio.Media.DATE_ADDED
        )   // TODO: throw more appropriate exception
            ?: throw NullPointerException("Failed to query content resolver for saved recordings")

        val idColumn        = cursor.getColumnIndex(MediaStore.Audio.Media._ID)
        val nameColumn      = cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME)
        val dateAddedColumn = cursor.getColumnIndex(MediaStore.Audio.Media.DATE_ADDED)

        // Columns that don't exist on all supported Android versions
        var durationColumn    = -1
        var dateTakenColumn   = -1
        var relativePathColum = -1

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            durationColumn    = cursor.getColumnIndex(MediaStore.Audio.Media.DURATION)
            dateTakenColumn   = cursor.getColumnIndex(MediaStore.Audio.Media.DATE_TAKEN)
            relativePathColum = cursor.getColumnIndex(MediaStore.Audio.Media.RELATIVE_PATH)
        }

        while (cursor.moveToNext()) {
            val recordingContentValues = ContentValues()
            recordingContentValues.put(MediaStore.Audio.Media.DISPLAY_NAME,  cursor.getString(nameColumn))
            recordingContentValues.put(MediaStore.Audio.Media.DATE_ADDED,    cursor.getLong(dateAddedColumn))

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                recordingContentValues.put(MediaStore.Audio.Media.DURATION,      cursor.getLong(durationColumn))
                recordingContentValues.put(MediaStore.Audio.Media.DATE_TAKEN,    cursor.getLong(dateTakenColumn))
                recordingContentValues.put(MediaStore.Audio.Media.RELATIVE_PATH, cursor.getString(relativePathColum))
            }

            recordingValues.add(recordingContentValues)
            recordingUris.add(
                ContentUris.withAppendedId(
                    MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    cursor.getLong(idColumn)
                )
            )
        }

        cursor.close()

        // Set the initialization flag
        initialized = true
    }
}

class RecordingsManager(context: Context) {
    var recordingsPath: String = (Environment.getExternalStoragePublicDirectory(
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            Environment.DIRECTORY_RECORDINGS
        } else {
            Environment.DIRECTORY_MUSIC
        }
    )?.absolutePath + "/Sound Recorder Plus")
    private val mContentResolver = context.contentResolver

    init {
        // Make sure the recording directory exists
        File(recordingsPath).mkdirs()

        // Initialize global recordings data
        if (!GlobalRecordingsData.initialized) {
            GlobalRecordingsData.init(context)
            migrateRecordings(context)
        }
    }

    private fun migrateRecordings(context: Context) {
        val externalFilesDir = context.getExternalFilesDir(null)
        if (externalFilesDir == null) {
            Log.i("RecordingsManager", "External files dir isn't available. Skipping migration")
            return
        }

        // First, check if there are any recordings in our recordings directory
        val oldRecordingsDirectory = File(externalFilesDir.absolutePath)

        // Sanity check
        if (!oldRecordingsDirectory.exists()) {
            Log.i("RecordingsManager", "External files dir does not exists. Skipping migration")
            return
        }

        // Then, open our old recordings database
        val recordingsDB = DBHelper(context)

        // Find a suitable file name (if any)
        var i = 0
        while (i < recordingsDB.getCount() || recordingsDB.getCount() > 0) {
            val currentItem: RecordingItem
            try {
                currentItem = recordingsDB.getItemAt(i)
            } catch (_: IndexOutOfBoundsException) {
                break
            }

            //// First, rename the file
            var newName = currentItem.name.substring(0, currentItem.name.lastIndexOf(".mp4")) + ".mp3"
            var newRecordingPath = File(recordingsPath, newName)

            // Guard against duplications
            var counter = 1
            while (newRecordingPath.exists()) {
                newName = newName.substring(
                    0,
                    newName.lastIndexOf(".mp3")
                ) + " (Migrated" + if (counter > 1) { " $counter" } else { "" } + ").mp3"
                newRecordingPath = File(recordingsPath, newName)

                counter++
            }

            //// Then add the recording to MediaStore
            var inputStream: FileInputStream? = null
            var outputStream: FileOutputStream? = null
            try {
                // What we're doing here is creating a new recording altogether. It doesn't seem like
                // we can just copy the recording under a different name and add it to MediaStore
                // that way; the new recordings directory isn't writable. Therefore, we'll add a
                // new recording via createRecording()
                //
                // The only "problem" about this method is that the creation date will be wrong, but
                // that's OK since I can excuse, "Technically, it wasn't added to MediaStore before
                // the migration :P"
                val descriptor = createRecording(newName)
                inputStream = FileInputStream("${currentItem.filePath}/${currentItem.name}")
                outputStream = FileOutputStream(descriptor.fileDescriptor)

                inputStream.copyTo(outputStream)

                finishRecording(newName, descriptor, currentItem.length)
            } catch (_: NullPointerException) {
                Log.e("RecordingsManager", "Migration of ${currentItem.name} failed with null URI")
                i++
                continue
            } catch (e: FileNotFoundException) {
                Log.e("RecordingsManager", "Failed to open ${currentItem.name}: ${e.message}")
            } catch (e: Exception) {
                Log.e("RecordingsManager", "Could not migrate recording ${currentItem.name}: ${e.message}")
                i++
                continue
            } finally {
                inputStream?.close()
                outputStream?.close()
            }

            //// Finally, delete the recording
            val oldRecordingPath = File(currentItem.filePath, currentItem.name)
            oldRecordingPath.delete()
            recordingsDB.removeItemWithId(currentItem.id)

            Log.i("RecordingsManager", "Successfully migrated recording ${currentItem.name}")
        }
    }

    fun getNumberOfRecordings(): Int {
        return GlobalRecordingsData.recordingValues.size
    }

    /** Creates a recording.
     *
     * This function adds a recording to media store and returns an opened ParcelFileDescriptor. The
     * returned ParcelFileDescriptor MUST be closed by the caller.
     *
     * Any error that occurs will have an exception thrown. createRecording never returns a null
     * FileDescriptor
     *
     * @param name The name of the recording
     *
     * @return Returns a ParcelFileDescriptor of the newly added recording.
     *
     */
    fun createRecording(name: String): ParcelFileDescriptor {
        val recordingValues = ContentValues()
        val currentTimeMillis = System.currentTimeMillis()
        val currentTimeSeconds = TimeUnit.MILLISECONDS.toSeconds(currentTimeMillis)

        recordingValues.put(MediaStore.Audio.Media.DISPLAY_NAME, name)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            recordingValues.put(MediaStore.Audio.Media.DATE_TAKEN, currentTimeMillis)
            recordingValues.put(
                MediaStore.Audio.Media.RELATIVE_PATH,
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    Environment.DIRECTORY_RECORDINGS
                } else {
                    Environment.DIRECTORY_MUSIC
                } + "/Sound Recorder Plus"
            )
        }

        // Required on Android < 10
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            recordingValues.put(MediaStore.Audio.Media.DATA, "$recordingsPath/$name")
        }

        recordingValues.put(MediaStore.Audio.Media.DATE_ADDED, currentTimeSeconds)

        val collection = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            MediaStore.Audio.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
        } else {
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        }

        val uri: Uri = mContentResolver.insert(collection, recordingValues)
            ?: throw NullPointerException("Recording insertion failed with null URI")
        GlobalRecordingsData.recordingUris.add(uri)
        GlobalRecordingsData.recordingValues.add(recordingValues)

        val fileDescriptor = mContentResolver.openFileDescriptor(uri, "w")
            ?: throw FileNotFoundException("Could not open $name")
        return fileDescriptor
    }

    /** Finishes a recording by updating its contents in MediaStore.
     *
     * This function simply fills in the length to add more metadata to MediaStore (if on Android
     * 10+) and closes the recording file descriptor automatically for you.
     *
     * @param name The name of the recording to finish. (This is used in case createRecording() was
     * called more than once).
     * @param fd The FileDescriptor of the open recording.
     * @param length The length of the recording (unused on Android < 10).
     */
    fun finishRecording(name: String, fd: ParcelFileDescriptor, length: Long) {
        // Add the duration metadata...but only if we're on Android 10+
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            var recordingValues: ContentValues? = null
            var recordingUri: Uri? = null
            for (i in 0..GlobalRecordingsData.recordingValues.size) {
                val currentValue = GlobalRecordingsData.recordingValues[i]
                if (currentValue.getAsString(MediaStore.Audio.Media.DISPLAY_NAME) == name) {
                    recordingValues = currentValue
                    recordingUri    = GlobalRecordingsData.recordingUris[i]
                    break
                }
            }

            if (recordingValues == null) {
                throw IndexOutOfBoundsException("Recording not found")
            }

            recordingValues.put(MediaStore.Audio.Media.DURATION, length)

            // Update MediaStore
            if (mContentResolver.update(recordingUri!!, recordingValues, null, null) == 0) {
                Log.e("RecordingsManager", "Recording not updated in MediaStore successfully")
                return
            }
        }

        // Finally, close the file descriptor
        fd.close()
    }

    /** Returns the database item of the recording at a given index.
     *
     * @param index The index of the recording item.
     * @return The information of a recording at a given index.
     */
    fun getRecording(index: Int): RecordingItem {
        val recording = RecordingItem()

        try {
            val recordingValues = GlobalRecordingsData.recordingValues[index]
            recording.name   = recordingValues.getAsString(MediaStore.Audio.Media.DISPLAY_NAME)
            recording.length = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                recordingValues.getAsLong(MediaStore.Audio.Media.DURATION)
            } else {
                val metadataRetriever = MediaMetadataRetriever()
                val recordingName = GlobalRecordingsData.recordingValues[index].getAsString(MediaStore.Audio.Media.DISPLAY_NAME)
                metadataRetriever.setDataSource("$recordingsPath/$recordingName")
                val length = metadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                    ?.toLong() ?: -1

                metadataRetriever.close()
                length
            }

            // DATE_TAKEN requires Android 10+, so use DATE_ADDED for now
            recording.time = recordingValues.getAsLong(MediaStore.Audio.Media.DATE_ADDED)
            recording.filePath = GlobalRecordingsData.recordingUris[index].path ?: throw NullPointerException("Recording path is null")
        } catch (_: IndexOutOfBoundsException) {
            throw IndexOutOfBoundsException("Recording at index $index doesn't exist")
        }

        return recording
    }

    fun getRecordingUri(index: Int): Uri {
        try {
            return GlobalRecordingsData.recordingUris[index]
        } catch (e: IndexOutOfBoundsException) {
            throw IndexOutOfBoundsException("Attempt to get non-existent recording URI at index $index")
        }
    }

    /** Checks if a recording name exists
     *
     * @param name The name of the recording to check
     */
    fun checkIfNameExists(name: String): Boolean {
        // Query all files from MediaStore
        val collectionUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            MediaStore.Audio.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
        } else {
            MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        }

        val projections = arrayOf(
            MediaStore.Audio.Media.DISPLAY_NAME,
        )

        val cursor = mContentResolver.query(
            collectionUri,
            projections,
            "${MediaStore.Audio.Media.DISPLAY_NAME} = ?",
            arrayOf(name),
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                MediaStore.Audio.Media.DATE_TAKEN
            } else {
                MediaStore.Audio.Media.DATE_ADDED
            }
        )   // TODO: throw more appropriate exception
            ?: throw NullPointerException("Failed to query content resolver for saved recording")

        if (cursor.moveToFirst()) {
            cursor.close()
            return true
        } else {
            cursor.close()
            return false
        }
    }

    /** Removes a recording from the database and deletes it from storage.
     * <p>
     * If deleting the recording fails, this method throw an IOException. It is
     * possible that the recording might have been removed from the database.
     * <p>
     * TODO: Better handle database errors. These are not handled very well in
     * DBHelper. Also make more non-null guarantees
     *
     * @param index The index of the recording
     */
    @Throws(IOException::class, IndexOutOfBoundsException::class)
    fun deleteRecording(index: Int) {
        try {
            // Remove the recording from MediaStore
            val deletedRows = mContentResolver.delete(GlobalRecordingsData.recordingUris[index], null, null)
            GlobalRecordingsData.recordingUris.removeAt(index)
            GlobalRecordingsData.recordingValues.removeAt(index)

            Log.i("RecordingsManager", "Deleted $deletedRows rows from MediaStore")
        } catch (exception: IndexOutOfBoundsException) {
            throw exception
        }
    }

    /** Renames a recording.
     * <p>
     * If the rename fails, this method throws an IOException.
     *
     * TODO: Remove @Throws annotation once all backend code is fully rewritten in Kotlin.
     *
     * @param index The index of the recording.
     * @param newName The new name of the recording.
     */
    @Throws(NullPointerException::class, IndexOutOfBoundsException::class)
    fun renameRecording(index: Int, newName: String) {
        try {
            val recordingValues = GlobalRecordingsData.recordingValues[index]
            val file = File(recordingsPath, GlobalRecordingsData.recordingValues[index].getAsString(MediaStore.Audio.Media.DISPLAY_NAME))
            val newFile = File(recordingsPath, newName)

            // Update the actual file name
            if (!file.renameTo(newFile)) {
                Log.e("RecordingsManager", "Failed to rename recording filename")
                return
            }

            // Update the file in MediaStore
            recordingValues.put(MediaStore.Audio.Media.DISPLAY_NAME, newName)
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                recordingValues.put(MediaStore.Audio.Media.DATA, "$recordingsPath/$newName")
            }

            if (mContentResolver.update(GlobalRecordingsData.recordingUris[index], recordingValues, null, null) == 0) {
                Log.e("RecordingsManager", "Recording not updated in MediaStore successfully")
                return
            }

            GlobalRecordingsData.recordingValues[index] = recordingValues
        } catch (exception: NullPointerException) {
            throw NullPointerException("Failed to get decoded path")
        } catch (exception: IndexOutOfBoundsException) {
            throw IndexOutOfBoundsException("Recording at $index doesn't exist")
        }
    }
}
