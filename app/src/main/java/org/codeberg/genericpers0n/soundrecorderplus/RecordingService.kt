package org.codeberg.genericpers0n.soundrecorderplus

import android.app.Service
import android.content.Intent
import android.media.MediaRecorder
import android.os.Build
import android.os.IBinder
import android.os.ParcelFileDescriptor
import android.util.Log
import android.widget.Toast
import java.io.IOException

private const val LOG_TAG = "RecordingService"

class RecordingService : Service() {
    companion object {
        @JvmStatic
        var listener: RecordingsListener? = null
    }

    private var mFileName: String = ""
    private var mFilePath: String = ""

    private var mRecorder: MediaRecorder? = null
    private lateinit var mRecordingFd: ParcelFileDescriptor
    private var mRecordingsManager: RecordingsManager? = null

    private var mStartingTimeMillis = 0L

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()

        mRecorder =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                MediaRecorder(applicationContext)
            } else {
                MediaRecorder()
            }

        mRecordingsManager = RecordingsManager(applicationContext)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startRecording()
        return START_STICKY
    }

    override fun onDestroy() {
        if (mRecorder != null) stopRecording()
        super.onDestroy()
    }

    private fun setFileNameAndPath() {
        var count = 0

        do {
            count++

            mFileName =
                "${getString(R.string.default_file_name)}_" +
                "$count.mp3"
            mFilePath = mRecordingsManager!!.recordingsPath
        } while (mRecordingsManager!!.checkIfNameExists(mFileName))
    }

    private fun startRecording() {
        setFileNameAndPath()
        mRecordingFd = mRecordingsManager!!.createRecording(mFileName)

        mRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        mRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
        mRecorder!!.setOutputFile(mRecordingFd.fileDescriptor)
        mRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
        mRecorder!!.setAudioChannels(1)

        val bitrate = getPrefBitrate(this)
        mRecorder!!.setAudioEncodingBitRate(bitrate)

        val sampleRate = getPrefSampleRate(this)
        mRecorder!!.setAudioSamplingRate(sampleRate)

        try {
            mRecorder!!.prepare()
            mRecorder!!.start()
            mStartingTimeMillis = System.currentTimeMillis()
        } catch(exception: IOException) {
            Log.e(LOG_TAG, "prepare() failed: ${exception.message}")
            mRecorder!!.reset()
        }
    }

    private fun stopRecording() {
        mRecorder?.stop() ?: return
        val elapsedTime = (System.currentTimeMillis() - mStartingTimeMillis)
        mRecorder!!.release()
        mRecorder = null
        Toast.makeText(
            this,
            "${getString(R.string.toast_recording_finish)} $mFilePath",
            Toast.LENGTH_LONG
        ).show()

        mRecordingsManager!!.finishRecording(mFileName, mRecordingFd, elapsedTime)
        listener?.onRecordingsChanged()
    }
}