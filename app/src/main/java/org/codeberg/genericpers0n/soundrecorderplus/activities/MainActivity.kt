package org.codeberg.genericpers0n.soundrecorderplus.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import org.codeberg.genericpers0n.soundrecorderplus.R
import org.codeberg.genericpers0n.soundrecorderplus.RecordingsManager
import org.codeberg.genericpers0n.soundrecorderplus.fragments.FileViewerFragment
import org.codeberg.genericpers0n.soundrecorderplus.fragments.RecordFragment
import kotlin.system.exitProcess

private const val PERMISSIONS_REQUEST = 0

class MainActivity : AppCompatActivity() {
    private var tabTitles: Array<String>? = null

    private inner class MyAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {
        override fun createFragment(position: Int): Fragment {
            if (position == 1) return FileViewerFragment.newInstance(position)
            return RecordFragment.newInstance(position)
        }

        override fun getItemCount(): Int {
            return tabTitles!!.size
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //// UI Initialization
        setContentView(R.layout.activity_main)

        // Setup tab titles
        tabTitles = arrayOf(
            getString(R.string.tab_title_record),
            getString(R.string.tab_title_saved_recordings)
        )

        val pager: ViewPager2 = findViewById(R.id.pager)
        pager.setAdapter(MyAdapter(this))

        val tabs: TabLayout = findViewById(R.id.tabs)
        val tabLayoutMediator = TabLayoutMediator(
            tabs,
            pager
        ) { tab: TabLayout.Tab, position: Int ->
            tab.setText(tabTitles!![position])
        }

        tabLayoutMediator.attach()
        setSupportActionBar(findViewById(R.id.toolbar))

        // Request permissions
        val requestedPermissions = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            arrayOf(Manifest.permission.RECORD_AUDIO)
        } else {
            arrayOf(
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        }

        // Register a callback to handle the user's response to our permission request(s)
        val permissionRequestLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                for (permission in permissions) {
                    // If we were denied any permission, display a dialog box
                    if (!permission.value) {
                        Log.e("MainActivity", "Required permission denied")
                        exitProcess(1)
                    }
                }
            }

        // Request the actual permission
        var grantedPermissions = 0
        for (permission in requestedPermissions) {
            when (
                ContextCompat.checkSelfPermission(
                    applicationContext,
                    permission
                )
            ) {
                PackageManager.PERMISSION_DENIED -> {
                    permissionRequestLauncher.launch(requestedPermissions)
                }
                else -> grantedPermissions++
            }
        }

        // Make sure we have 3 granted permissions. If not, exit
        if ((Build.VERSION.SDK_INT <  Build.VERSION_CODES.Q && grantedPermissions != 3) ||
            (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && grantedPermissions != 1)) {
            Log.e("MainActivity", "Not all required permissions granted (FIXME).")
            exitProcess(1)
        }

        //// Initialize global recordings data
        RecordingsManager(applicationContext)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_REQUEST) {
            if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Log.e("SoundRecorderPlus", "REQUEST_MEDIA denied")
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        if (item.itemId == R.id.action_settings) {
            val i = Intent(this, SettingsActivity::class.java)
            startActivity(i)
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}
