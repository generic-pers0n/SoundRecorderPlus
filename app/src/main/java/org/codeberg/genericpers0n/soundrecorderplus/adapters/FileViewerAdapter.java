package org.codeberg.genericpers0n.soundrecorderplus.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.codeberg.genericpers0n.soundrecorderplus.R;
import org.codeberg.genericpers0n.soundrecorderplus.RecordingItem;
import org.codeberg.genericpers0n.soundrecorderplus.RecordingService;
import org.codeberg.genericpers0n.soundrecorderplus.RecordingsListener;
import org.codeberg.genericpers0n.soundrecorderplus.RecordingsManager;
import org.codeberg.genericpers0n.soundrecorderplus.fragments.PlaybackFragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Daniel on 12/29/2014.
 */
public class FileViewerAdapter extends RecyclerView.Adapter<FileViewerAdapter.RecordingsViewHolder>
    implements RecordingsListener {

    private static final String LOG_TAG = "FileViewerAdapter";

    private final RecordingsManager mRecordingsManager;

    RecordingItem item;
    Context mContext;
    final LinearLayoutManager llm;

    public FileViewerAdapter(Context context, LinearLayoutManager linearLayoutManager) {
        super();
        mContext = context;
        mRecordingsManager = new RecordingsManager(mContext);
        RecordingService.setListener(this);
        llm = linearLayoutManager;
    }

    @Override
    public void onBindViewHolder(final RecordingsViewHolder holder, int position) {

        item = getItem(position);
        long itemDuration = item.getLength();

        long minutes = TimeUnit.MILLISECONDS.toMinutes(itemDuration);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(itemDuration)
                - TimeUnit.MINUTES.toSeconds(minutes);

        holder.vName.setText(item.getName());
        holder.vLength.setText(
            String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds)
        );
        holder.vDateAdded.setText(
            DateUtils.formatDateTime(
                mContext,
                item.getTime() * 1000L,
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_YEAR
            )
        );

        // define an on click listener to open PlaybackFragment
        holder.cardView.setOnClickListener(view -> {
            try {
                PlaybackFragment playbackFragment =
                        new PlaybackFragment().newInstance(getItem(holder.getBindingAdapterPosition()));

                FragmentTransaction transaction = ((FragmentActivity) mContext)
                        .getSupportFragmentManager()
                        .beginTransaction();

                playbackFragment.show(transaction, "dialog_playback");

            } catch (Exception e) {
                Log.e(LOG_TAG, "exception", e);
            }
        });

        holder.cardView.setOnLongClickListener(v -> {

            ArrayList<String> entries = new ArrayList<>();
            entries.add(mContext.getString(R.string.dialog_file_share));
            entries.add(mContext.getString(R.string.dialog_file_rename));
            entries.add(mContext.getString(R.string.dialog_file_delete));

            final CharSequence[] items = entries.toArray(new CharSequence[0]);


            // File delete confirm
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle(mContext.getString(R.string.dialog_title_options));
            builder.setItems(items, (dialog, item) -> {
                if (item == 0) {
                    shareFileDialog(holder.getBindingAdapterPosition());
                }
                if (item == 1) {
                    renameFileDialog(holder.getBindingAdapterPosition());
                } else if (item == 2) {
                    deleteFileDialog(holder.getBindingAdapterPosition());
                }
            });
            builder.setCancelable(true);
            builder.setNegativeButton(mContext.getString(R.string.dialog_action_cancel),
                    (dialog, id) -> dialog.cancel());

            AlertDialog alert = builder.create();
            alert.show();

            return false;
        });
    }

    @NonNull
    @Override
    public RecordingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.card_view, parent, false);

        mContext = parent.getContext();

        return new RecordingsViewHolder(itemView);
    }

    public static class RecordingsViewHolder extends RecyclerView.ViewHolder {
        protected final TextView vName;
        protected final TextView vLength;
        protected final TextView vDateAdded;
        protected final View cardView;

        public RecordingsViewHolder(View v) {
            super(v);
            vName = v.findViewById(R.id.file_name_text);
            vLength = v.findViewById(R.id.file_length_text);
            vDateAdded = v.findViewById(R.id.file_date_added_text);
            cardView = v.findViewById(R.id.card_view);
        }
    }

    @Override
    public int getItemCount() {
        return mRecordingsManager.getNumberOfRecordings();
    }

    public RecordingItem getItem(int position) {
        return mRecordingsManager.getRecording(position);
    }

    @Override
    public void onRecordingsChanged() {
        //item added to top of the list
        notifyItemInserted(getItemCount() - 1);
        llm.scrollToPosition(getItemCount() - 1);
    }

    public void remove(int position) {
        //remove item from database, recyclerview and storage
        try {
            RecordingItem item = getItem(position);
            mRecordingsManager.deleteRecording(position);

            Toast.makeText(
                    mContext,
                    String.format(
                            mContext.getString(R.string.toast_file_delete),
                            item.getName()
                    ),
                    Toast.LENGTH_SHORT
            ).show();
            notifyItemRemoved(position);
        } catch (IOException exception) {
            Toast.makeText(
                mContext,
                String.format(
                    mContext.getString(R.string.toast_file_delete_failure),
                    getItem(position).getName()
                ),
                Toast.LENGTH_SHORT
            ).show();
        }
    }

    public void rename(int position, String name) {
        try {
            mRecordingsManager.renameRecording(position, name);
            notifyItemChanged(position);
        } catch (Exception exception) {
            Toast.makeText(
                mContext,
                String.format(
                    mContext.getString(R.string.toast_recording_rename_failure),
                    exception.getLocalizedMessage()
                ),
                Toast.LENGTH_SHORT
            ).show();
        }
    }

    public void shareFileDialog(int position) {
        Intent shareIntent = new Intent();
        Uri fileUri = mRecordingsManager.getRecordingUri(position);

        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        shareIntent.setType("audio/mp3");
        mContext.startActivity(Intent.createChooser(shareIntent, mContext.getText(R.string.send_to)));
    }

    public void renameFileDialog (final int position) {
        // File rename dialog
        AlertDialog.Builder renameFileBuilder = new AlertDialog.Builder(mContext);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.dialog_rename_file, null);

        EditText input = view.findViewById(R.id.new_name);

        // Populate the file name in the input
        String fileNameNoExtension = mRecordingsManager.getRecording(position).getName();
        fileNameNoExtension = fileNameNoExtension.substring(0, fileNameNoExtension.lastIndexOf(".mp3"));
        input.setText(fileNameNoExtension);

        renameFileBuilder.setTitle(mContext.getString(R.string.dialog_title_rename));
        renameFileBuilder.setCancelable(true);
        renameFileBuilder.setPositiveButton(mContext.getString(R.string.dialog_action_ok),
                (dialog, id) -> {
                    try {
                        String value = input.getText().toString().trim() + ".mp3";
                        rename(position, value);

                    } catch (Exception e) {
                        Log.e(LOG_TAG, "exception", e);
                    }

                    dialog.cancel();
                });
        renameFileBuilder.setNegativeButton(mContext.getString(R.string.dialog_action_cancel),
                (dialog, id) -> dialog.cancel());

        renameFileBuilder.setView(view);
        AlertDialog alert = renameFileBuilder.create();
        alert.show();
    }

    public void deleteFileDialog (final int position) {
        // File delete confirm
        AlertDialog.Builder confirmDelete = new AlertDialog.Builder(mContext);
        confirmDelete.setTitle(mContext.getString(R.string.dialog_title_delete));
        confirmDelete.setMessage(mContext.getString(R.string.dialog_text_delete));
        confirmDelete.setCancelable(true);
        confirmDelete.setPositiveButton(mContext.getString(R.string.dialog_action_yes),
                (dialog, id) -> {
                    try {
                        //remove item from database, recyclerview, and storage
                        remove(position);

                    } catch (Exception e) {
                        Log.e(LOG_TAG, "exception", e);
                    }

                    dialog.cancel();
                });
        confirmDelete.setNegativeButton(mContext.getString(R.string.dialog_action_no),
                (dialog, id) -> dialog.cancel());

        AlertDialog alert = confirmDelete.create();
        alert.show();
    }
}
