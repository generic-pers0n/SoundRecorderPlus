package org.codeberg.genericpers0n.soundrecorderplus

import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator

class RecordingItem() : Parcelable {
    var name: String = ""
    var filePath: String = ""

    // FIXME: Convert these to their unsigned counterparts once the rewrite is complete
    var id: Int = -1
    var length: Long = -1
    var time: Long = -1

    @Suppress("unused")
    @JvmField
    val CREATOR: Creator<RecordingItem?> = object : Creator<RecordingItem?> {
        override fun createFromParcel(inParcel: Parcel): RecordingItem {
            return RecordingItem(inParcel)
        }

        override fun newArray(size: Int): Array<RecordingItem?> {
            return arrayOfNulls(size)
        }
    }

    constructor(inParcel: Parcel) : this() {
        name = inParcel.readString() ?: ""
        filePath = inParcel.readString() ?: ""
        id = inParcel.readInt()
        length = inParcel.readLong()
        time = inParcel.readLong()
    }

    constructor(name: String, filePath: String, id: Int, length: Long, time: Long) : this() {
        this.name     = name
        this.filePath = filePath
        this.id       = id
        this.length   = length
        this.time     = time
    }

    // Overrides //////////////////////////////////////////////////////////////
    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(id)
        dest.writeLong(length)
        dest.writeLong(time)
        dest.writeString(filePath)
        dest.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }
}
